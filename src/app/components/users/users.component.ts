import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { MatTableDataSource } from '@angular/material';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {

  usersData:string[] = null;
  loadingUsers:boolean = true;

  name:string;
  lastName:string;
  email:string;
  address:string;
  phone:string;

  displayedColumns: string[] = ['_id', 'name', 'lastName', 'email', 'address', 'phone'];
  dataSource = null;

  constructor(
  	private router: Router,
  	private usersService: UsersService
  ) {

  	this.usersService.getAllUsers()
  		.pipe(
        	map((data:any) => {
        		if (data) {
        			this.loadingUsers = false;
        			this.usersData = data;
        			this.dataSource = new MatTableDataSource(data.usersList);
        		}
        })).subscribe((data):any=> {});
  }

  searchUsers(event) {
  	const value = event.target.value;
  	this.dataSource.filter = value.trim().toLowerCase();
  }

}
