import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { UsersService } from '../../services/users.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss']
})
export class NewuserComponent implements OnInit {

  newUserForm: FormGroup;
  submitted:boolean = false;
  createdUser:boolean = false;
  phoneOutput:string = '';

  constructor(
  	private router: Router,
  	private usersService: UsersService,
  	private formBuilder: FormBuilder
  ) {
  	this.createForm();
  }

  createForm() {
  	 this.newUserForm = this.formBuilder.group({
  		name: ['', [Validators.required, Validators.pattern('[a-zA-Z]+')]],
  		lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z]+')]],
  		email: ['', [Validators.required, Validators.pattern(this.emailPattern())]],
  		address: ['', Validators.required],
  		phone: ['', [Validators.required, Validators.minLength(14)]],
  	});
  }

  get f() { return this.newUserForm.controls; }

  ngOnInit() {
  }

  emailPattern() {
  	return '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';
  }

  inputPhonePress(event:any) {
    const value = event.target.value;
    const code = (event.which) ? event.which : event.keyCode;

    if (code === 16 && event.keyCode !== 8) {
      event.preventDefault();
    }

    if ((code < 48 || code > 57) && event.keyCode !== 8) {
      event.preventDefault();
      return;
    }
  }

  phoneInput(event:any) {
  	let value = event.target.value;
  	let formated = '';

  	if (event.keyCode != 8) {
      value = value.replace(/[^0-9]/g, '');

      const area = value.substr(0, 3);
      const initial = value.substr(3, 3);
      const last = value.substr(6, 4);

      if (area.length < 3) {
        formated = `(${area}`;
      } else if (area.length == 3 && initial.length < 3) {
        formated = `(${area}) ${initial}`;
      } else if (area.length == 3 && initial.length == 3) {
        formated = `(${area}) ${initial}-${last}`;
      }

      this.phoneOutput = formated;
    }
  }

  onSubmit() {
  	this.submitted = true;

  	if (this.newUserForm.invalid) {
      return;
    }

    const obj:object = {...this.newUserForm.value };

    return this.usersService.createUser(obj)
    	.pipe(
        	map((data:any) => {
        		if (data) {
        			this.createdUser = true;
        			this.submitted = false;
        			this.newUserForm.reset();

        			setTimeout(() => {
        				this.createdUser = false;
					}, 3500);
        		}
        })).subscribe((data):any=> {});
  	}
}
