import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UsersService {

  constructor(
  	private http: HttpClient
  ) { }

  public urlPath:string = 'http://206.189.221.85:3002/softtek/';

  getAllUsers() {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  	return this.http.get(`${this.urlPath}getUsers`, {headers:headers});
  }

  createUser(obj:object) {
  	const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  	return this.http.post(`${this.urlPath}createUser`, JSON.stringify(obj),  {headers:headers})
  }
}
